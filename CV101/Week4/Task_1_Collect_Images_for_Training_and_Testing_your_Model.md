<img src="images/IDSNlogo.png" width="300">

### Task 1: Collect Images for Training and Testing your Model

In order to train your Visual Recognition Classifier, you need to gather datasets of images for people wearing Helmet or No-helmet. Collecting the right and diversified data is a very important component for building a good vision recognition model. To help you in finding the images, we are proposing the following sources and you can search for more sources. 

Google

Kaggle

Github

Unsplash 


During your image collection process, we recommend you: 

1. Collect at least 160 images for each set of labels (that is 160 for Helmet, 160 for No-helmet)

2. Ensure you have a diverse set of images in your dataset. For example:

- Different colors of Helmets (White, Orange, Red,...)
- Different camera angles of the people wearing Helmet (from the sides, back, and front)
- Different times of day ( night, sunny,...)
3. Keep around 5% (approx. 10 per label) of the collected images for testing the model

4. For the remainder, 95% of the images (that is, about 150 per label) that will be used for training the model, create two different ".zip" files. Call them Helmet.zip and No-helmet, respectively. This will make it easy for drag-and-drop into Watson Visual Recognition.

Proceed to the next Task, once you have gathered and saved sufficient number of images and saved then in the zip files as indicated above.

