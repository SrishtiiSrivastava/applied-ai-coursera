<img src="images/IDSNlogo.png" width="300"/>

### Task 3: Part A - Overview

The instruction for deploying your model as a Web App on the cloud is found in the next section. In the next section you will launch a Jupyter notebook, which contains the instructions on how to deploy your Helmet Classifier Web App on the cloud.

After deploying your Web App to the cloud, you will be able to access your Visual Recognition classifier as a webpage, similar to the image below.

<img src="images/4.3.PNG" width="600"/>



