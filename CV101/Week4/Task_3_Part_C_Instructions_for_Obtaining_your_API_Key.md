<img src="images/IDSNlogo.png" width="300"/>

### Task 3: Part C - Instructions for Obtaining your API Key

The following are the instructions to obtain the API Key for your Watson Visual Recognition API

First login to your IBM Cloud account.

https://cloud.ibm.com

Then under "services", click on your Watson Visual Recognition Service. Here the Watson Visual Recognition Service is called "watson-vision-combined-dsx"

<img src="images/4.3.3.1.PNG" width="600">

This will take you to your Watson Visual Recognition service, where you will find your API Key

<img src="images/4.3.3.2.PNG" width="600">

Thank you.