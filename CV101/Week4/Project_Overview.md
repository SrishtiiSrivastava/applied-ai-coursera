### Project Overview

In the previous modules, you learned how to train a custom Visual Recognition classifier. For the project in this module, you will develop a new custom classifier and learn how to deploy it as a web app on the cloud. There are various advantages to deploying your Visual Recognition classifier to the cloud. First, you can share your classifier with anyone in the world. They simply need to enter the URL of your model into any web browser. Second, you can showcase your web app in your portfolio, and your potential future employers can interact with your project.

#### Project Scenario
Imagine you are an AI Application Developer in one of the recently eastablished oil and gas plants. Lately, the safety advisor has noticed an increase in the head injuries among the plant's operators and workers. Luckily, majority of the head injuries are minor but it resulted in an increase in the health insurance premium by 10%. The only way to resolve the problem is to monitor and ensure that each person entering the plant is wearing a helmet and keeping them on. There are many cameras installed in the plant. So, the plant management and safety advisor asked you if you can use the cameras to create a computer vision solution that can flag anyone not wearing a helmet. You are excited because you know you can do this quite easily and quickly using IBM Watson Visual Recognition.

#### Project Tasks
You will start by developing a visual recognition model that will classify the images of the plant workers as wearing a Helmet or No-Helmet. You will utilize Watson Studio and the Watson Visual Recognition service to develop this model and then deploy it as a web app by completing the tasks below.

Task 1: Gathering the Dataset

Task 2: Training your Classifier

Task 3: Deploying your Web App

Task 4: Testing your Classifier

Task 5: Submit your Assignment and Evaluate your Peers (for non-audit i.e. paid learners only)

Each of the above tasks will be described in more detail in the sections that follow.




