<img src="images/IDSNlogo.png" width="300">

### Task 2: Training your Classifier

After you obtained your dataset of images, you should use that dataset to train your Classifier on Watson Visual Recognition, which you learned to do in the previous section.

The following is the link to IBM Watson Visual Recognition.

<a href=https://cloud.ibm.com/registration>IBM Watson Visual Recognition</a>