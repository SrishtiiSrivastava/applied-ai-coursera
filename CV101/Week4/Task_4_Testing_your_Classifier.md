<img src="images/IDSNlogo.png" width="300"/>

### Task 4: Testing your Classifier

After you deploy your Visual Recognition classifier, anyone can classify images of people with Helmet and without Helmet using your classifier using the URL of your web app. All they have to do is enter the URL into any browser and upload an image. And they will get a classification of the image along with its class score.

<img src="images/4.4.1.PNG" width="600">

Your mark for this project will depend on how well your Web App correctly classifies images of people with Helmet or without Helmet.