<img src="images/IDSNlogo.png" width="300"/>

# LAB: Classifying Images with IBM Watson (30 min)


In this hands-on exercise you will utilize *IBM Watson Visual Recognition* (VR) to upload and classify your images. Watson VR is a service that uses deep learning algorithms to identify objects, faces and other content in an image. Follow the steps below to complete this exercise.

NOTE: In order to complete this exercise you will be creating an IBM Cloud account and provisioning an instance for Watson Visual Recognition service. A credit card is NOT required to sign up for IBM Cloud Lite account and there is no charge associated in creating a Lite plan instance of the Watson VR service.

## STEP 1: CREATE AN IBM CLOUD ACCOUNT


Click on the link below to create an IBM cloud account:

<a href=https://cloud.ibm.com/registration>Sign Up for IBM Watson Visual Recognition on IBM Cloud</a>

## Sign Up for IBM Watson Visual Recognition on IBM Cloud

On the page to which you get redirected by clicking on the link above, enter your **email address, first name, last name, country or region**, and set your **password**.

NOTE: To get enhanced benefits, please sign up with you **company email** address rather than a free email ID like Gmail, Hotmail, etc.

If you would like IBM to contact you for any changes to services or new offerings, then check the box to accept the box to get notified by email. Then click on the Create Account button to create your IBM Cloud account.

**If you already have an IBM Cloud account you can just log in using the link above the Email field (top right in the screenshot below).


<img src="images/Lab_6.1.1__1.png" width="600">



## STEP 2: CONFIRM YOUR EMAIL ADDRESS

An email is sent to your email address to confirm your account.


<img src="images/Lab_6.1.1__2.png" width="600">


Go to your email account, and click on the "**Confirm Account**" link in the email that was sent to you.


<img src="images/Lab_6.1.1__3.png" width="600">


## STEP 3: LOGIN TO YOUR ACCOUNT


<img src="images/Lab_6.1.1__4.png" width="600">


When you click ​<a href=https://cloud.ibm.com/>Log in</a>, you will be redirected to a page to log into your IBM Cloud account.


<img src="images/Lab_6.1.1__5.png" width="600">


## STEP 4: CREATE A NEW RESOURCE

On your dashboard page, click on the **Create a resource** on the top right to create a new source.


<img src="images/Lab_6.1.1__6.png" width="600">


## STEP 5: CREATE A VISUAL RECOGNITION AND WATSON STUDIO RESOURCE

On the **Catalog** page, select the **AI** category from the left pane, and then select the **Visual Recognition** resource.


<img src="images/Lab_6.1.1__7.png" width="600">


On the next page, you will get to name your service instance and choose your region. Click on the arrow to reveal the drop-down menu of regions. Make sure to select the region that is closest to you. Since I am located in Canada, then I am choosing **Dallas** as my region since it is the closest region to me.


<img src="images/Lab_6.1.1__8.png" width="600">


On the **Catalog** page, select the **AI** category from the left pane, and then select the **Watson Studio** resource.

<img src="images/Lab_6.1.1__9.png" width="600">


On the next page, you will get to name your service instance and choose your region. Click on the arrow to reveal the drop-down menu of regions. Make sure to select the region that is closest to you. Since I am located in Canada, then I am choosing **Dallas** as my region since it is the closest region to me.


<img src="images/Lab_6.1.1__10.png" width="600">


Then scroll down and make sure that the **lite** plan is selected, and click the **Create** button.


<img src="images/Lab_6.1.1__11.png" width="600">


On the next page, click the **Get Started button** to start using Watson Studio.


<img src="images/Lab_6.1.1__12.png" width="600">


This will start provisioning your Watson Studio instance.

<img src="images/Lab_6.1.1__13.png" width="600">


Once the provisioning process is complete, click the **Get Started** button to start using Watson Studio.

## STEP 6: CREATE A PROJECT

Once you land on the IBM Watson Studio main page, start by creating a project.


<img src="images/Lab_6.1.1__14.png" width="600">


Create a **Visual Recognition project**.


<img src="images/Lab_6.1.1__15.png" width="600">


## STEP 7: SETTING UP YOUR PROJECT

Now let's fill in some project details and click **Create**. The IBM Cloud Object Storage, which provides you storage for your images, should be automatically created for you.

<img src="images/Lab_6.1.1__16.png" width="600">

Create the storage service with the lite plan.

<img src="images/Lab_6.1.1__17.png" width="600">

Go to **Add to project** and choose **Image classification**.

<img src="images/Lab_6.1.1__18.png" width="600">

Then provision a visual recognition service.

<img src="images/Lab_6.1.1__19.png" width="600">

## STEP 8: SELECTING BUILT-IN MODELS FOR WATSON VISUAL RECOGNITION

After creating your project, by default, you will land on the page where you can create your own custom models to classify objects -- but let's skip this step. Instead, let's use some existing image classification models that Watson Visual Recognition comes with by default!

To access the built-in models, click on the name of the service, as seen in the red box below:

<img src="images/Lab_6.1.1__20.png" width="600">

## STEP 9: CHOOSE THE GENERAL MODEL.

Now you can see all the built-in image classification models that IBM Watson provides! Let's try the **General** model.

<img src="images/Lab_6.1.1__21.png" width="600">

## STEP 10: TRY OUT THE GENERAL MODEL

To test the General model, click on **Test**.

<img src="images/Lab_6.1.1__22.png" width="600">

## STEP 11: UPLOAD YOUR IMAGES!

Now you can upload any images you'd like by clicking on **Browse**.

<img src="images/Lab_6.1.1__23.png" width="600">

##STEP 12: CHECK OUT THE RESULTS!

Once you have uploaded your images, Watson Studio Visual Recognition will tell you what it thinks it found in your images! Beside each class of object (or color, age, etc.), it will also give you a confidence score (between 0 and 1) on how confident it thinks it found that particular object in your image (0 for lowest confidence and 1 for highest confidence).

<img src="images/Lab_6.1.1__24.png" width="600">

## STEP 13: FILTER YOUR RESULTS

<img src="images/Lab_6.1.1__25.png" width="600">

## STEP 14: SHARE YOUR RESULTS!

Follow us on Twitter and send us some of your funniest and most interesting results you found with IBM Watson Visual Recognition!

