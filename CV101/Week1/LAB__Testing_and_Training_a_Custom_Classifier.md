<img src="images/IDSNlogo.png" width="300"/>

# IBM Watson Visual Recognition


IBM Watson Visual Recognition is a service that uses deep learning algorithms to identify objects, faces and other contents in an image.

You can sign up by clicking the following link.

1. https://cloud.ibm.com/registration

2. On the signup page, select where you want your data to be stored in, in my case it is **Dallas**. Enter your email address, then check the checkbox that is surrounded by the red rectangular border indicated in the picture below. Then click the **Next** button.

<img src="images/Lab_6.3__1.png" width="600"/>

3. After clicking **Next** you should end up on a page similar to the picture below. If you already have an IBM Cloud account you can just log in. But if you do not have an IBM Cloud account, you can fill in your profile information, such as your Email, First Name, Last Name, Country or Region, and set your Password. Then click on the **Create Account** button to create your IBM Cloud account.


<img src="images/Lab_6.3__2.png" width="600"/>

4. After this you will see the below image. In a new tab, open and login to your email account


<img src="images/Lab_6.3__3.png" width="600"/>

5. Open the IBM Cloud confirmation email and click on **Confirm Account**.


<img src="images/Lab_6.3__4.png" width="600"/>

6. Congratulations, you have successfully activated your IBM Cloud account. Upon clicking Confirm account you will see the following screen (i.e. you are automatically logged in). If you are automatically logged in the manner below, click on **Get Started** and then **click Create a Project. (Skip to Step 10)**

<img src="images/Lab_6.3__5.png" width="600"/>

7. OR the following screen will show where you have to manually type in your username and password to log in


<img src="images/Lab_6.3__6.png" width="600"/>

8. After logging in, search for **Watson Studio** and click on it. You will then see

<img src="images/Lab_6.3__7.png" width="600"/>

9. Upon clicking it, you will see the following screen. Click on **Create**


<img src="images/Lab_6.3__8.png" width="600"/>

After clicking on Create, you will see the following screen. Click on **Get Started**

<img src="images/Lab_6.3__9.png" width="600"/>

10. After clicking Get Started you will see the following screen


<img src="images/Lab_6.3__10.png" width="600"/>

11. Click on **Create a Project**

12. You will then see the following screen. **Click on Create an empty project**


<img src="images/Lab_6.3__11.png" width="600"/>

13. Give your project a name and select the following checkbox


<img src="images/Lab_6.3__12.png" width="600"/>

14. Scroll down and click on **Add**


<img src="images/Lab_6.3__13.png" width="600"/>

15. A new tab will open which will prompt you to add a Cloud Object Storage instance to your project. To make sure you don't already have one in your account do the following. Click on **existing** and then click on **Resource Group**. Select **ALL** checkboxes from the dropdown.


<img src="images/Lab_6.3__14.png" width="600"/>

16. Similarly, click on **Location** and select **ALL checkboxes** from the dropdown

<img src="images/Lab_6.3__15.png" width="600"/>

17. If an existing Cloud Object Storage instance shows up, select it and skip to **Step 20**. If **not**, proceed to the next step.

18. Click on **New** on the top left corner beside Existing. Upon clicking it you will see the following screen. Scroll to the bottom and select the Lite Plan. Then click on **Create**


<img src="images/Lab_6.3__16.png" width="600"/>

19. Upon clicking Create you will see the following screen. Click **Confirm**

<img src="images/Lab_6.3__17.png" width="600"/>

20. After this the new tab will close and you will return to your Project tab. Here click on **Refresh**


<img src="images/Lab_6.3__18.png" width="600"/>

21. After clicking Refresh, it will show you the Cloud Object Storage Instance name that you either just created with the Lite plan or will show the name of an existing one and if you selected that one for this project. Click **Create**


<img src="images/Lab_6.3__19.png" width="600"/>

22. After clicking Create you will see the following screen. Click on the **X** to close the popup


<img src="images/Lab_6.3__20.png" width="600"/>

23. Click on **Add to Project**

<img src="images/Lab_6.3__21.png" width="600"/>

24. Select the **image classification model**


<img src="images/Lab_6.3__22.png" width="600"/>

25. After selecting it, you will see the following screen prompting you to add a Visual Recognition service to your project. Click on **here**


<img src="images/Lab_6.3__23.png" width="600"/>

26. After clicking here, you will see the following screen.


<img src="images/Lab_6.3__24.png" width="600"/>

Repeat **steps 15 - 19** but this time we are doing it for the visual recognition service. That is, click on **Existing** under the Visual Recognition service, select **ALL** checkboxes under the **Resources** and **Location** dropdown menus and check if an existing Visual Recognition service shows up. If it does select it, else switch to **New** under the Visual Recognition Service, select the Lite Plan and click Create. After clicking **Create**, you will see a popup. Click **Confirm** on it. After performing these steps, you will see the following screen. You can also click on your Associated Service name under **Default Custom Model** to see the list of available classifiers

<img src="images/Lab_6.3__25.png" width="600"/>

For the custom model that we are creating in the visual recognition service, we are classifying 3 breeds of dogs, Husky, Beagle and GoldenRetriever. The dataset is given in the links below:

- https://cocl.us/CV0101EN_Coursera_Beagle

- https://cocl.us/CV0101EN_Coursera_GoldenRetriever

- https://cocl.us/CV0101EN_Coursera_Husky

So let's start by creating our first class for training.

<img src="images/Lab_6.3__26.png" width="600"/>

Let's start with Husky, so we name our first-class Husky.

<img src="images/Lab_6.3__27.png" width="600"/>

Let’s upload our training dataset for Husky, click on **Browse**, and select the Husky.zip file which contains the training images for Husky.

<img src="images/Lab_6.3__28.png" width="600"/>

To add the uploaded dataset to our model, click on the checkbox next to Husky.zip and then click on **Add to Model**.

<img src="images/Lab_6.3__29.png" width="600"/>

Now do the same with the dataset for Beagle and GoldenRetriever. Then click Train Model.

<img src="images/Lab_6.3__30.png" width="600"/>

Now that your model is trained click on Trained.

<img src="images/Lab_6.3__31.png" width="600"/>

To test our model click on the **Test** option, then browse to upload your test data set.

<img src="images/Lab_6.3__32.png" width="600"/>

Once you uploaded your images, **Watson Studio Visual Recognition** will give each image an associated tag of a class that the image falls under. Besides each tag there is a confidence score of how sure the model thinks that the image matches the tag.


<img src="images/Lab_6.3__33.png" width="600"/>

