<img src="images/IDSNlogo.png" width="200" height="200"/>

## Objectives

### Hands on Assignment Part 1: IBM Cloud Service Creation(15min)

Objective for Exercise:
* How to create an IBM Cloud account.
* How to create a Watson Studio Service instance.

**Note:** A credit card is NOT required to sign up for an IBM Cloud Lite account and there is no charge associated in creating a Lite plan instance. 

 
### Exercise 1: Create an IBM Cloud Account

#### Scenario

To access the resources and services that the IBM Cloud provides, you need an IBM Cloud account. 

If you already have an IBM Cloud account, you can skip Exercise 1, login to your IBM cloud account and proceed with *Exercise 2: Add Watson Studio as a resource.*

#### Task 1: Sign up for IBM Cloud

1.  Go to: [https://cloud.ibm.com/registration](https://cloud.ibm.com/registration) to create a free account on IBM Cloud.

Note: If you already have an IBM Cloud account, Login by going to [https://cloud.ibm.com/login](https://cloud.ibm.com/login) and skip ahead to Exercise 2.

2. Enter your company **Email** address and a strong **Password** and then click the **Next** button.

<img src="images/1-email.jpg"  width="600">

3. An email is sent to the address that you signed up with to confirm your email address. Check your email and copy and paste **Verification code**. Then click **Next**.

<img src="images/2-verify_email.jpg"  width="600">

4. Once your email is successfully verified, enter your **First name**, **Last name**, and **Country** and click **Next**.

<img src="images/3-personal_info.jpg"  width="600">

5. After your personal information is accepted Click on **Create account** button.

<img src="images/4-create_account.jpg"  width="600">

 *It takes a few seconds to create and set up your account.*

6. You will see this page which confirms your account creation and allows you to login.
<img src="images/account_setup_screen.png"  width="600">


7. The username (which is your email address) is already populated. Enter your password and login.
<img src="images/login_screen.png"  width="600">

8. Read carefully about the IBMid Privacy and click on proceed to acknowledge and login.
<img src="images/account_privacy.png"  width="600">

9. Once you successfully login, you should see the dashboard. You can now proceed to the next exercise.
<img src="images/dashboard.png"  width="600">


### Exercise 2: Create an instance of Watson Studio service 

#### Scenario

Watson Studio, which is available as a service on IBM Cloud, and a component of the IBM Cloud Pak for Data, allows you to build and collaborate on AI and Data Science projects. In this exercise, you will add an instance of the Watson Studio service to your IBM Cloud account.

#### Task 1: Add Watson Studio as a resource

1. In the IBM Cloud **Catalog** go to the Watson Studio listing - [cloud.ibm.com/catalog/services/watson-studio](https://cloud.ibm.com/catalog/services/watson-studio)
<img src="images/7-create_project.jpg"  width="600">


2. On the Watson Studio page, select DALLAS as the Region, verify that the **Lite** plan is selected, and then click **Create**.

<img src="images/5-create_watson_studio_lite.jpg"  width="600">


3. Once the Watson Studio instance is successfully created, click on **Get Started**.

<img src="images/6-watson_studio_get_started.jpg"  width="600">

4. Once you get started, the first time, your IBM Cloud Pak for Data core services is provisioned.

<img src="images/provisioning.png"  width="600">

5. You will then see the list of cloud services to be initialized. Click on **Continue** to proceed.

<img src="images/ready_to_launch.png"  width="600">

6. The cloud services will be initialized. This will take a few seconds. Once done, you will see a button which says **Go to IBM Cloud Pak for Data**

<img src="images/cloud_services_ready.png"  width="600">

7. You will see a welcome screen. You may choose to click on next and view the suggestions or close it and get started.

<img src="images/flashscreen.png"  width="600">

In the next Hands on lab exercise, you will be creating a project on Watson Studio and will learn how to classify hotel reviews using a Data Modeler.

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2021-06-03 | 2.0 | Srishti | Created lab |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
