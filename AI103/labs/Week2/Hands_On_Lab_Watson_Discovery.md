<img src="images/IDSNlogo.png" width="300"/>

# Hands-on Lab: Watson Discovery (10 min)

Objective for Exercise:
* To make you familiar with Watson Discovery Service.

## Exploring Watson Discovery
IBM provides an online demo of Watson Discovery at [Discovery](https://www.ibm.com/demos/live/watson-discovery/self-service/home)
Use the following steps to explore the demo:

1. Access the online demo here: [Discovery](https://www.ibm.com/demos/live/watson-discovery/self-service/home)

2. The demo contains **Car Manual** collection.

3. Under **Discovery search** click on the dorpdown and  select _How to properly install child restraints_ 

<img src="images/WD_demo1.png" width="600"/>

4. Scroll down and read the answers Watson discovery found from the collection, you can also read the most relevant answer by looking at the confidence score.

**Explanation on Traditional search and Discovery search:**

Traditional enterprise search and search engines don’t provide employees and customers with exact answers. They can’t understand the nuances of phrases and acronyms in your industry and accurately search through your complex documents in a timely manner. 

Watson Discovery solves these challenges. It is enterprise search that delivers specific answers to your queries while also serving up the entire document and supporting links, allowing your employees and customers to make informed decisions with confidence.

5. Now analyse the difference in the Discovery search and Traditional search.

6. Go and explore other given options for the **Car manual** collection:

<img src="images/WD_demo2.png" width="600"/>

7. Click on the answer to open the document and read the **Discovery result detail** 

8. Explore Watson Discovery capabilities and read how Watson came up with these results by clicking on **Next Attribute**

<img src="images/WD_demo3.png" width="600"/>

## Author(s)
[Srishti Srivastava](linkedin.com/in/srishti-srivastava-343095a8)


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-10-16 | 2.0 | Srishti | Migrated Lab to Markdown and added to course repo in GitLab |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>








