<img src="images/IDSNlogo.png" width="300"/>

## Hands-on Lab - Visual Recognition

Objective for Exercise:
* Exploring Watson Visual Recognition
* Understanding Pre Trained Models
* Learn how to use Watson Visual Recognition to analyze images.

## Hands-on Lab - Visual Recognition (10 min)

Exploring Watson Visual Recognition

IBM provides an online demo of Watson Visual Recognition at: [Watson
Visual
Recognition](https://visual-recognition-code-pattern.ng.bluemix.net/)

There is a pre-trained Visual recognition model which analyzes and identifies the object. 

Use the following steps to explore the demo:

**Pre Trained Models**

**Step I**

1.  By default, the image of the person in the tweed jacket is selected.

2.  In the output section on the right, Note that Watson has identified characteristics that exist in the image.

3.  What level of confidence does Watson have that the image is of:

4.  fabric

5.  gray color

6.  Harris Tweed (jacket)

7.  clothing

8.  Select the image of liquid in a beaker.

9.  Under General Model, review the options. Watson can identify characteristics that exist in the image.

10. What level of confidence does Watson have that the image is of:

11. chocolate color

12. beverage

13. food

**Step II**

1. Select the given two images (hard disk and beaker) and analyze the result.

2. Take and save screenshot of the **beaker**.

3. Upload the screenshot in the final assignment, the screenshot includes the picture of the beaker you selected along with the labels and confidence scores.


## Author(s)
[Srishti Srivastava](https://www.linkedin.com/in/srishti-srivastava-343095a8/)



## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-10-26 | 2.0 | Srishti | Created Lab for VR demo |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
