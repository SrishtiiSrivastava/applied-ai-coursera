<img src="images/IDSNlogo.png" width="300"/>

## Exercise: Analyzing documents with Watson Discovery!

In this hands-on exercise you will utilize Watson Discovery to extract valuable information from documents. When handling an enormous amount of text documents, Watson Discovery is a valuable tool to optimize your work flow by applying Natural Language Processing (NLP) algorithm to extract, valuable information, sentiment, concepts, semantic roles from your collection of documents.

NOTE: For learners working to get a certificate for this course (i.e. not auditing), please go through this exercise carefully and thoroughly it will be used for the graded assignment.

FYI: In order to complete this exercise you will be creating an IBM Cloud account and provisioning an instance for Watson Discovery service. A credit card is NOT required to sign up for IBM Cloud Lite account and there is no charge associated in creating a Lite plan instance of the Watson Discovery service.

#### Step 1: Create an IBM Cloud Account

Click on the link below to create an IBM cloud account:

<a href=https://cloud.ibm.com/registration> Sign Up for IBM Watson Visual Recognition on IBM Cloud</a>

On the page to which you get redirected by clicking on the link above, enter your email address, first name, last name, country or region, and set your password.

NOTE: To get enhanced benefits, please sign up with you business or corporate email address rather than a free email ID like Gmail, Hotmail, etc.

If you would like IBM to contact you for any changes to services or new offerings, then check the box to accept the box to get notified by email. Then click on the Create Account button to create your IBM Cloud account.

**If you already have an IBM Cloud account you can just log in using the link above the Email field (top right in the screenshot below).

<img src="images/4.1.png" width="600">

#### Step 2: Confirm Your Email Address

An email is sent to your email address to confirm your account.

<img src="images/4.2.png" width="600">

Go to your email account, and click on the **Confirm Account** link in the email that was sent to you.

<img src="images/4.3.png" width="600">

#### Step 3: Login to Your Account

<img src="images/4.4.png" width="600">

When you click ​<a href=https://cloud.ibm.com/>Log in</a>, you will be redirected to a page to log into your IBM Cloud account.

<img src="images/4.5.png" width="600">

#### Step 4: Create a New Resource

On your dashboard page, click on the Create a resource on the top right to create a new source.

<img src="images/4.6.png" width="600">

#### Step 5: Create a Watson Discovery Resource
On the **Catalog** page, select the **AI** category from the left pane, and then select the **Watson Discovery** resource.

<img src="images/4.7.png" width="600">

On the next page, you will get to name your service instance and choose your region. Click on the arrow to reveal the drop-down menu of regions. Make sure to select the region that is closest to you. Since I am located in Canada, then I am choosing Dallas as my region since it is the closest region to me

<img src="images/4.8.png" width="600">

Then scroll down and make sure that the lite plan is selected, and click the **Create** button.

<img src="images/4.9.png" width="600">
This will start provisioning your Watson Discovery instance. Once the service has been provisioned, you can get into your Watson Discovery instance by clicking the Discovery instance under the Services tab.

<img src="images/4.10.png" width="600">

Launch your service by clicking "Launch Watson Discovery".

<img src="images/4.11.png" width="600">

#### Step 6: Setting up Your Collection

Your collection will be hosted in a Collection, click on "Upload your own data" to create your collection.

<img src="images/4.12.png" width="600">

You will be prompted to upgrade to the Advance plan, but for this exercise you only need the Lite plan, so you could Set up with current plan.

<img src="images/4.13.png" width="600">

Since we are creating a collection for our documents, you can give your Collection a name, and set the language of your documents to English.

<img src="images/4.14.png" width="600">

#### Step 7: Uploading your Documents

For the purpose of this lab, we will be analyzing IBM's 2018 Annual Report, you can download the report from the following link.

<a href = https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/IntroductionToAI/IBM_Annual_Report_2018.pdf/>IBM_Annual_Report_2018.pdf </a>

Upload the IBM 2018 Annual Report onto Watson Discovery.

Click on "select document" and choose the IBM 2018 Annual Report.

Note! You can upload documents in the form of PDF, HTML, JSON, WORD, Excel, PowerPoint, PNG, TIFF, JPG. Any of the above filetypes for up to 50 megabytes.

<img src="images/4.15.png" width="600">

#### Step 8: Analyzing your Document

After your document has finished processing, click on the "Schema" tag highlighted by the red box, and click on document view. As you scroll down you can see the sentiment level of each entity. In this case it is positive for IBM Watson, IBM Services, IBM Cloud, and AI. So this tool has improved my workflow by providing me the sentiment for IBM Watson, IBM Services, and IBM Cloud without someone reading through the whole document.

<img src="images/4.16.png" width="600">

#### Step 9: Querying your Document
You can also extract important information from your document by using keywords. Click on the Search tab, and enter your keywords in the Search for documents section. In this case we are searching for "Intellectual Property", Watson Discovery has returned the result for "Intellectual Property" in our document.

<img src="images/4.17.png" width="600">

### Step 10: Save a screen shot
NOTE: This step is Optional for those Auditing the course. The screenshot saved in this step will be required as part of the Graded Final Assignment for those pursuing a certificate for this course.

From Steps 8 take and save a screenshot in .jpeg or .jpg format including the Watson Discovery sentiment analysis of the document. See a sample screenshot below.

<img src="images/4.18.png" width="600">

#### Step 15: Share Your Results!
Follow us on Twitter and send us some of your funniest and most interesting results you found with IBM Watson Visual Recognition!

<img src="images/4.19.jpg" width="600">

<a href=https://twitter.com/intent/tweet>Click here</a> to share the above Tweet.

<a href=https://twitter.com/ravahuja>Follow Rav Ahuja </a>
